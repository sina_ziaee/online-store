package com.example.onlinestore.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.onlinestore.R;

public class AdminPanelActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_panel);
    }
}
