
package com.example.onlinestore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.onlinestore.Activities.AdminPanelActivity;
import com.example.onlinestore.Activities.DownloadsActivity;
import com.example.onlinestore.Activities.FavoriteActivity;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private EditText et_search;
    private ImageView img_toggle;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeFields();
        setListeners();
    }

    private void initializeFields() {
        toolbar = findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);
        navigationView = findViewById(R.id.main_navigation_view);
        img_toggle = findViewById(R.id.toggle_navigation_menu);
        et_search = findViewById(R.id.et_main_search);
        drawerLayout = findViewById(R.id.drawer_main);
    }

    private void setListeners() {
        navigationView.setNavigationItemSelectedListener(this);
        img_toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        Intent intent = null;

        switch (id){
            case R.id.nav_favorite:
                intent = new Intent(MainActivity.this, FavoriteActivity.class);
                break;
            case R.id.nav_downloads:
                intent = new Intent(MainActivity.this, DownloadsActivity.class);
                break;
            case R.id.nav_admin_panel:
                intent = new Intent(MainActivity.this, AdminPanelActivity.class);
                break;
        }

        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }

        startActivity(intent);

        return true;
    }

    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
